# GraphQL API
Simple example how to build GraphQL API with Rails

### Dependencies

**Development**

* Ruby 2.6.3 ([rbenv](https://github.com/rbenv/rbenv#installing-ruby-versions))
* Rails 6.0.0 ([rails guides](https://guides.rubyonrails.org/6_0_release_notes.html))

### How to start application on your dev machine
1) Install above mentioned dependencies
2) Install gems with `bundle`
2) `bundle exec rake db:create db:migrate db:seed`
3) Run your server with `rails s` 🚀

### How to make a query
Fetch the titles of all posts that are authored by Piotr

Query:
```
query fetchPosts($authorName: String!) {
	author(name: $authorName) {
		posts {
			title
		}
	}
}
```

Query variables:
```
{
	"authorName": "Piotr"
}
```

### How to fire a mutation
Create a very first post of Eamon

Mutation:
```
mutation createPost($input: CreatePostInput!) {
	createPost(input: $input) {
		post {
			authorId
			title
		}
	}
}
```

Input:
```
{
	"input": {
		"post": {
			"authorId": 2,
			"title": "Welcome everybody"
		}
	}
}
```
