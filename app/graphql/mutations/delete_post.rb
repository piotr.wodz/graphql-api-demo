module Mutations
	class DeletePost < GraphQL::Schema::RelayClassicMutation
		argument :id, ID, required: true

		field :errors, [String], null: false

		def resolve(id:)
			post_object = DeletePostService.new(id).call

			{ errors: post_object.errors.full_messages }
		end
	end
end
