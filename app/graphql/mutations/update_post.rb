module Mutations
	class UpdatePost < GraphQL::Schema::RelayClassicMutation
		argument :post, Types::PostInputType, required: true

		field :post, Types::PostType, null: false
		field :errors, [String], null: true

		def resolve(post:)
			post_object = UpdatePostService.new(post.to_h).call

			{ post: post_object, errors: post_object.errors.full_messages }
		end
	end
end
