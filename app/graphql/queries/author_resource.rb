module Queries
	class AuthorResource < Queries::BaseQuery
		type Types::AuthorType, null: true
		argument :name, String, required: true

		def resolve(name:)
			Author.find_by(name: name)
		end
	end
end
