module Queries
	class PostResource < Queries::BaseQuery
		type Types::PostType, null: true
		argument :id, ID, required: true
		
		def resolve(id:)
			Post.find_by(id: id)
		end
	end
end
