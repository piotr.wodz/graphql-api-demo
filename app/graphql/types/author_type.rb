module Types
	class AuthorType < Types::BaseObject
		field :id, Int, null: true
		field :name, String, null: false
		field :age, Integer, null: true

		field :posts, [Types::PostType], null: true
	end
end
