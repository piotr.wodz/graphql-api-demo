module Types
	class PostInputType < Types::BaseInputObject
		argument :id, Int, required: false
		argument :author_id, Int, required: true
		argument :title, String, required: true
		argument :body, String, required: false
	end
end
