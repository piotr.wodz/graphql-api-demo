module Types
	class PostType < Types::BaseObject
		field :id, Int, null: true
		field :author_id, Int, null: false
		field :title, String, null: false
		field :body, String, null: true
	end
end
