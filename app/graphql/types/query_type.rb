module Types
  class QueryType < Types::BaseObject
    field :author, resolver: Queries::AuthorResource
    field :post, resolver: Queries::PostResource
  end
end
