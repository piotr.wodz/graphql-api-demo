class CreatePostService
	attr_reader :params
	
	def initialize(params)
		@params = params
	end
	
	def call
		Post.create(params)
	end
end
