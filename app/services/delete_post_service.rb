class DeletePostService
	attr_reader :id

	def initialize(id)
		@id = id
	end

	def call
		Post.find(id).destroy
	end
end
