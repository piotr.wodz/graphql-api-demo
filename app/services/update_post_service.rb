class UpdatePostService
	attr_reader :params

	def initialize(params)
		@params = params
	end

	def call
		post = Post.find(params.delete(:id))
		post.update(params)

		post
	end
end
