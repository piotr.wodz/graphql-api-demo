# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

piotr = Author.create!(
	name: 'Piotr',
	age: 27
)

eamon = Author.create!(
	name: 'Eamon',
	age: 35
)

vivian = Author.create!(
	name: 'Vivian',
	age: 26
)

Post.create!(
	author: piotr, 
	title: 'GraphQL introduction',
	body: 'To be continued...'
)

Post.create!(
	author: piotr,
	title: 'Vue.js vs React',
	body: 'I should write something in here'
)

Post.create!(
	author: vivian, 
	title: 'My adventure in North America',
	body: 'Travelling is cool'
)